package ee.bcs.valiit.w1;

import java.util.LinkedList;
import java.util.List;

public class Ex420 {

    static List<String> names = new LinkedList<>();

    public static void main(String[] args) {
        names.add("Pontu");
        names.add("Pitsu");
        names.add("Mumuu");
        names.add("Lassie");
        names.add("Muri");
        names.add("Muki");
        names.add("KahKoer");
        int i = 0;
        while (i < names.size()) {
            System.out.println(names.get(i));
            i++;
        }
    }

}
