package ee.bcs.valiit.w1;

public class Types {

    public static void main(String[] args) {
        byte a = 10, b = 12;
        Byte aa = 14, bb = 7;
        System.out.println("a + b = " + (a + b));
        System.out.println("a + aa = " + (a + aa));
        a += 5;
        a--;
        System.out.println("a != aa ? " + (a != aa));
        double c = 9.8;
        Double cc = 0.2;
        System.out.println("c + cc = " + (c + cc));

        int i = (int) c;
        System.out.println("c -> i ? " + i);

        String d = "1024";
        //Integer dd = Integer.getInteger(d);
        //Integer dd = (Integer) d;
        Integer dd = Integer.parseInt(d);

        System.out.println("d + 1 = " + (d + 1));
        System.out.println("dd + 1 = " + (dd + 1));

        int h = 10;
        int j = 18;
        int k = 17;

        System.out.println("h % 3 + j % k = " + (h % 3 + j% k));

        String planet1 = "Merkuur";
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Jupiter";
        String planet56 = "Saturn, Uran";
        String[] otherPlanets = planet56.split(", ");
        otherPlanets[1] = "Neptuun";
        int planetCount = 6;
        System.out.println(
                String.format("%s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti",
                        planet1,
                        planet2,
                        planet3,
                        planet4,
                        otherPlanets[0],
                        otherPlanets[1],
                        planetCount));

        StringBuilder planets = new StringBuilder();
        planets.append("Merkuur, ")
                .append("Venus, ")
                .append("Maa on Päikesesüsteemi 3 planeeti");

        System.out.println(planets.toString());

        System.out.println("Raamatu \"Rehepapp\" on\n\t\\A. Kivirähk\\");

    }

}
