package ee.bcs.valiit.w1;

public class Ex417 {

    public static void main(String[] args) {
        int i = 0;
        // for (int i = 0; i < args.length; i++) { // tavaline for-tsükkel
        for (String arg : args) { // for-each
            switch (arg) {
                case "0":
                    System.out.print("null");
                    break;
                case "1":
                    System.out.print("üks");
                    break;
                case "2":
                    System.out.print("kaks");
                    break;
                default:
                    System.err.println("Error!");
            }
            if (i < args.length - 1) {
                System.out.print(", ");
            } else {
                System.out.println(".");
            }
            i++;
        }
    }

}
