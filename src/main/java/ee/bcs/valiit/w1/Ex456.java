package ee.bcs.valiit.w1;

public class Ex456 {


    public static void main(String[] args) {
        int vanus = 100;
        System.out.println(vanus > 100 ? "Vana" : "Noor"); // ülesanne 5 lahendus
        System.out.println(vanus > 100 ? "Vana" : vanus == 100 ? "Peaaegu vana" : "Noor"); // ülesanne 6 lahendus
    }


}
