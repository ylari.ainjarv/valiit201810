package ee.bcs.valiit.w1;

public class Ex11 {

    static boolean test(int i, int j) {
        if (i < 10)
            return false;
        else
            return true;
    }

    public static void main(String[] args) {
        System.out.println("arv on < 10 -> " + test(5, 4));
        System.out.println("arv on > 10 -> " + test(12, 6));
    }

}
