package ee.bcs.valiit.w1;

import java.util.ArrayList;
import java.util.List;

public class Ex419 {

    static List<String> cities = new ArrayList<>();

    public static void main(String[] args) {
        cities.add("Tallinn");
        cities.add("Kärdla");
        cities.add("Pärnu");
        cities.add("Paide");
        cities.add("Keila");
        System.out.println(String.format("%s, %s ja %s",
                cities.get(0),
                cities.get(2),
                cities.get(cities.size() - 1)));
    }

}
