package ee.bcs.valiit.w1;

public class Colors {

    public static void main(String[] args) {
        if (args[0].equalsIgnoreCase("green")) {
            System.out.println("Driver can drive a car.");
        } else if (args[0].equalsIgnoreCase("yellow")) {
            System.out.println("Driver has to be ready to stop the car or to start driving.");
        } else if (args[0].equalsIgnoreCase("red")) {
            System.out.println("Driver has to stop car and wait for green light.");
        } else {
            System.err.println("Error!");
        }

        switch (args[0].toLowerCase()) {
            case "green":
                System.out.println("Driver can drive a car.");
                break;
            case "yellow":
                System.out.println("Driver has to be ready to stop the car or to start driving.");
                break;
            case "red":
                System.out.println("Driver has to stop car and wait for green light.");
                break;
            default:
                System.err.println("Error!");

        }

        System.out.println("Tavaline tekst ...");
        System.err.println("Veatekst ...");

    }

}
