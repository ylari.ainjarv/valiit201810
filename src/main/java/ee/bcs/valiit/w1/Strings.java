package ee.bcs.valiit.w1;

public class Strings {

    public static void main(String[] args) {
        String s1 = "Ülari";
        String s2 = "Ainjärv";
        String s3 = "ülari Ainjärv";
        String s4 = s1 + " " + s2;
        System.out.println(s3);
        System.out.println(s4);
        System.out.println("Kas on võrdsed? " + (s3 == s4));
        System.out.println("Tegelikult ju on - " +(s3.equalsIgnoreCase(s4)));

        int i = 10;
        int j = 10;
        System.out.println("Aga mis värk nendega on? " + (i == j));
    }
}
