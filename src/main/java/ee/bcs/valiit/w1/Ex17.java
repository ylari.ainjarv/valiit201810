package ee.bcs.valiit.w1;

public class Ex17 {

    static int deriveBirthYear(String idCode) {
        char sajand = idCode.charAt(0);
        StringBuilder synniaasta = new StringBuilder();
        switch (sajand) {
            case '1':
            case '2':
                synniaasta.append("18");
                break;
            case '3':
            case '4':
                synniaasta.append("19");
                break;
            case '5':
            case '6':
                synniaasta.append("20");
                break;
            case '7':
            case '8':
                synniaasta.append("21");
                break;
            default:
                synniaasta.append("Pole võimalik :)");
        }
        synniaasta.append(idCode.substring(1, 3));
        return Integer.parseInt(synniaasta.toString());
    }

    public static void main(String[] args) {
        System.out.println(deriveBirthYear("37212210230"));

        int i = 51;
        System.out.println("i == 3 ? " + (i == '3'));
    }
}
