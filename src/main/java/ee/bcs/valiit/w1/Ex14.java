package ee.bcs.valiit.w1;

public class Ex14 {

    static int[] test(int i, int j, boolean b) {
        if (j == 0) {
            int[] ret1 = new int[1];
            ret1[0] = i;
            return ret1;
        }
        int[] ret = new int[2];
        ret[0] = i;
        ret[1] = j;
        return ret;
    }

    static int[] test(int i, boolean b) {
        return test(i, 0, b);
    }

    public static void main(String[] args) {
        boolean b = false;
        Integer i = Integer.parseInt(args[0]), j;
        if (args.length > 1) {
            b = true;
            j = Integer.parseInt(args[1]);
            System.out.println("Length: " + test(i, j, b).length);
        } else {
            System.out.println("Length: " +test(i, b).length);
        }

    }

}
