package ee.bcs.valiit.w1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AI {

    public static void main(String[] args) {
        try {
            int[] m = new int[2];
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Sisesta esimene vanus: ");
            String s = br.readLine();
            m[0] = Integer.parseInt(s);
            System.out.print("Sisesta teine vanus: ");
            s = br.readLine();
            m[1] = Integer.parseInt(s);
            Arrays.sort(m);
            int v = m[1] - m[0];
            if (v > 10)
                System.out.println("Midagi veel krõbedamat ...");
            else if (v > 5)
                System.out.println("Midagi krõbedat ...");
            else if (v <= 5)
                System.out.println("Sobib!");
        } catch (IOException e) {
            System.err.println("Error!");
        }
    }

}
