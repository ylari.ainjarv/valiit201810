package ee.bcs.valiit.w1;

public class Ex13 {

    public static final double VAT = 1.2;

    static double addVat(double sum) {
        double total = sum * VAT;
        return total;
    }

    public static void main(String[] args) {
        System.out.println("100 + KM = " + (addVat(100)));
        System.out.println("78 + KM = " + (addVat(78)));
    }

}
