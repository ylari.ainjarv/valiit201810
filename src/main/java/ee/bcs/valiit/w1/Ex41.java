package ee.bcs.valiit.w1;

import java.math.BigInteger;

public class Ex41 {


    public static void main(String[] args) {
        BigInteger big = new BigInteger("39486734867394873948734875938476589345893465689346589346589346589469823465246");
        BigInteger big2 = new BigInteger("394876032784567203876023486776457845784655784657846578465786458746578647856478568746");
        System.out.println("big + big1 = " + big.add(big2));

        byte a = 0x30; // 16-süsteem
        byte b = 0b00110000; // 2-süsteem
        byte c = 060; // 8-süsteem
        byte d = 48; // 10-süsteem
        byte e = '0'; // sümbol
        System.out.println(a == b && a == c && a == d && a == e);

    }

}
