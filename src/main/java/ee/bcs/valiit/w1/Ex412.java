package ee.bcs.valiit.w1;

public class Ex412 {

    public static void main(String[] args) {
        int[] mas = new int[100];
        for (int i = 1; i <= 100; i++) {
            mas[i - 1] = i;
        }
        for (int i = 100; i > 0; i--) {
            System.out.println(mas[i - 1]);
        }
    }

}
