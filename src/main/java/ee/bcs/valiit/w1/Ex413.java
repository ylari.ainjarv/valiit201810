package ee.bcs.valiit.w1;

public class Ex413 {

    public static void main(String[] args) {
        int[] m = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int x: m) {
            System.out.println(x);
        }
    }

}
