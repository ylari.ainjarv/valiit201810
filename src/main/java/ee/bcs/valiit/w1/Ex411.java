package ee.bcs.valiit.w1;

public class Ex411 {

    public static void main(String[] args) {
        int i = 1;
        while (i <= 100) {
            System.out.println(i);
            i++;
        }

        i = 0;
        while (true) {
            if (i == 100)
                break;
            System.out.print(i);
            if (i % 10 == 0)
                System.out.print('\n');
            i++;
        }

        boolean onVeelKaupa = false;

        do {
            System.out.println("Ikka teen, mis sest et kaupa pole");
        } while (onVeelKaupa);

    }

}
