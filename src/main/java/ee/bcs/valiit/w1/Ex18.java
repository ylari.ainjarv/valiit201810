package ee.bcs.valiit.w1;

import java.math.BigInteger;

public class Ex18 {

    static boolean validatePersonalCode(BigInteger idCode) {
        String idCodeStr = idCode.toString();
        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int check = 0;
        for (int i = 0; i < weights1.length; i++) {
            check += Integer.parseInt(idCodeStr.substring(i, i + 1))
                    * weights1[i];
        }
        int temp = check % 11;
        String idCodeCheck = idCodeStr.substring(10, 11);
        if (temp != 10) {
            return Integer.parseInt(idCodeCheck) == temp;
        } else {
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            for (int i = 0; i < weights2.length; i++) {
                check += Integer.parseInt(idCodeStr.substring(i, i + 1))
                        * weights2[i];
            }
            temp = check % 11;
            if (temp != 10) {
                return Integer.parseInt(idCodeCheck) == temp;
            } else {
                return Integer.parseInt(idCodeCheck) == 0;
            }
        }
    }

    public static void main(String[] args) {
        BigInteger idCode = new BigInteger("37212210232");
        System.out.println(validatePersonalCode(idCode));
    }

}
