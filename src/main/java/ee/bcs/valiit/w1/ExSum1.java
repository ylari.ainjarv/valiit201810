package ee.bcs.valiit.w1;

import org.apache.commons.lang.StringUtils;

public class ExSum1 {

    public static void main(String[] args) {

        for (int i = 0; i < 6; i++) {
            for (int j = 6; j > i; j--) {
                System.out.print("#");
            }
            System.out.println();
        }


        for (int i = 0; i < 6; i++)
            System.out.println(StringUtils.repeat("#", i));
    }

}
