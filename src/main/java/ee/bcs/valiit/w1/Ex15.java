package ee.bcs.valiit.w1;

public class Ex15 {

    static char deriveGender(String idCode) {
        return (int) idCode.charAt(0) % 2 == 0 ? 'F' : 'M';
    }

    public static void main(String[] args) {
        System.out.println(String.format("Sugu on %c",
                deriveGender(args[0])));
    }

}
