package ee.bcs.valiit.w1;

public class Ex415 {

    public static void main(String[] args) {
        String[] bands = {"Sun", "Metsatöll", "Queen", "Meallica"};
        int i = 0;
        for (String band : bands) {
            System.out.print(band.equalsIgnoreCase("meallica")
                    ? "Metallica" : band);
            if (i < bands.length - 1) {
                System.out.print(", ");
            }
            i++;
        }
    }

}
