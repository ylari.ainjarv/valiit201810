package ee.bcs.valiit.w1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Ex422 {

    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<>();
        String[] estCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        map.put("Estonia", estCities);
        String[] sweCities = {"Stockholm", "Uppsala", "Lund", "Köping"};
        map.put("Sweden", sweCities);

        // Enesepiinamine
        System.out.println("Enesepiinamine:");
        Object[] keys = map.keySet().toArray();
        for (int i = 0; i < keys.length; i++) {
            String key = (String) keys[i];
            System.out.println("Country: " + key);
            System.out.println("Cities:");
            String[] values = map.get(key);
            for (int j = 0; j < values.length; j++) {
                System.out.println("\t" + values[j]);
            }
        }

        // Variant 1
        System.out.println("Variant 1:");
        for (Map.Entry<String, String[]> country : map.entrySet()) {
            System.out.println("Country: " + country.getKey());
            System.out.println("Cities:");
            for (String city : country.getValue()) {
                System.out.println("\t" + city);
            }
        }

        // Variant 2
        System.out.println("Absoluutne tõde:");
        map.forEach((k, v) -> {
            System.out.println("Country: " + k);
            System.out.println("Cities:");
            Arrays.stream(v).forEach(c -> System.out.println("\t" + c));
        });
    }

}
