package ee.bcs.valiit.w1;

public class Expressions {

    public static void main(String[] args) {
        Integer a = Integer.parseInt(args[0]);
        //int a = 8;
        if (a % 2 == 0) {
            System.out.println("Arv " + a + " on paaris");
        } else {
            System.out.println("Arv " + a + " on paaritu");
        }

        System.out.println("Arv " + a + " on "
                + ((a % 2 == 0)
                ? "paaris"
                : "paaritu"));

        System.out.println(String.format("Arv %d on %s",
                a,
                a % 2 == 0 ? "paaris" : "paaritu"));
    }

}
