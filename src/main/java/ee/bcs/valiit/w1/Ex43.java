package ee.bcs.valiit.w1;

public class Ex43 {

    public static void main(String[] args) {
        int hinne = 5; // võimalikud väärtused 1 - 5
        if (hinne == 1) {
            System.out.println("nõrk");
        } else if (hinne == 2) {
            System.out.println("mitterahuldav");
        } else if (hinne == 3) {
            System.out.println("rahuldav");
        } else if (hinne == 4) {
            System.out.println("hea");
        } else if (hinne == 5) {
            System.out.println("suurepärane");
        } else {
            System.err.println("Error!");
        }
    }

}
